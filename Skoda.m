    %%
    clear all
    clc
    close all
%% load the car dataset given by palash which has other and our car data
    load Car_Dataset.mat
%% Get the feature/ variable names
Other_features = Other.Properties.VariableNames;
Our_features = Our.Properties.VariableNames;
%% Obtain the common variable names from both the datasets
common_features = intersect(Other_features,Our_features);
%% Remove the NaN and missing values and make datasets equal in rows.
Other = Other(907:1267,:);
Our = Our(907:end,:);
%% Get the plots for comparison between Other and Our's
    for i = 1:1:numel(common_features)
        temp = common_features{i};
        if isnumeric(eval(strcat('Other.',temp)))
            figure,
            plot(eval(strcat('Other.',temp)),'Linewidth',2);
            Other_Mat(:,i) = eval(strcat('Other.',temp)); % get the features for correlation
            hold on
            plot(eval(strcat('Our.',temp)),'Linewidth',2);
            Our_Mat(:,i) = eval(strcat('Our.',temp)); % get the features for correlation
            hold off
            title(temp);
            legend('Other', 'Ours');
            axis tight
            pause(0.1)
           
        end
      end

%% Computer correlations

correl = corr(Our_Mat);  %% get the correlations on Our car
figure, imagesc(correl), colorbar    %% all correlations
correl(correl<0.7) = 0; %% correlations greater than 0.7
figure, imagesc(correl), colorbar    %% correlations >0.7
correl(isnan(correl)) = 0; %% remove NaNs
[row, col] = size(correl); %% Get the rows and cols where 0.7 and greater correlation
mask = 1-eye(row); %% mask to remove diagonal entries (self correlation)
correl = mask.*correl; %% remove self correlations
figure, imagesc(correl), colorbar    %% final correlations 

%% Find the unique correlations

[r_ind, c_ind] = find(correl); 
[~,rc_ind_unique] = unique(sum([r_ind c_ind],2));

%% Print correlation variables

clc
for i = 1:1:length(rc_ind_unique)

   fprintf('%s <=> %s\n', common_features{r_ind(i)},common_features{c_ind(i)});  

end

%%


