%% global settings
close all
clear all
clc
%% import old and new car data
old = importfile('RPR-545_trackLog-2017-Mar-05_16-07-14.csv');
old(892,:) = []; % delete tis row since it is a repeated header
new = importfile('MMY-278_trackLog-2017-Mar-05_16-06-42.csv');
new(774,:) = []; % delete tis row since it is a repeated header
%% Get the plots for comparison between Old and New Car

variable{1} = 'EngineLoad';
variable{2} = 'EngineCoolantTemperatureC';
variable{3} = 'EngineRPMrpm';
variable{4} = 'SpeedOBDkmh';
% EngineOilTemperature doesnot have any numbers;
for i = 1:1:3
            figure,
            plot(eval(strcat('old.',variable{i})),'Linewidth',2);
            Old_Mat(:,i) = eval(strcat('old.',variable{i})); % get the features for correlation
            hold on
            plot(eval(strcat('new.',variable{i})),'Linewidth',2);
            New_Mat(:,i) = eval(strcat('new.',variable{i})); % get the features for correlation
            hold off
            title(variable{i});
            if(strcmp(variable{i},'EngineLoad'))
                hline_1 = refline([0 60]);
                hline_1.Color = 'k';
                hline_2 = refline([0 80]);
                hline_2.Color = 'k';
                legend('Old', 'New', 'Threshold_line');
            elseif(strcmp(variable{i},'EngineCoolantTemperatureC'))
                hline_1 = refline([0 40]);
                hline_1.Color = 'k';
                legend('Old', 'New', 'Threshold_line');
            elseif(strcmp(variable{i},'EngineRPMrpm'))
                hline_1 = refline([0 5000]);
                hline_1.Color = 'k';
                hline_2 = refline([0 6000]);
                hline_2.Color = 'k';
                hline_3 = refline([0 1300]);
                hline_3.Color = 'k';
                legend('Old', 'New', 'Threshold_line');
            end
                
            axis tight
            pause(0.1)
  
end


%%

Speed_old_OBD = (strrep(old.SpeedOBDkmh, '''', ''));
Speed_new_OBD = (strrep(new.SpeedOBDkmh, '''', ''));
Speed_old_OBD(strcmp('-', Speed_old_OBD))={0};

for i = 1:1:min(size(new,1),size(old,1))
    if (isnumeric(Speed_old_OBD{i,1}) && isnumeric(Speed_new_OBD{i,1}))
        S_old(i,:) = Speed_old_OBD{i,1};
        S_new(i,:) = Speed_new_OBD{i,1};
    else
        S_old(i,:) =  str2double(Speed_old_OBD{i,1});
        S_new(i,:) = str2double(Speed_new_OBD{i,1});
    end
end

%%
plot(S_old,'Linewidth',2);
hold on
plot(S_new,'Linewidth',2);
hold off
title(variable{4});            
 legend('Old', 'New');           