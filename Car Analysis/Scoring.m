%%
Freeway_or_motorway = mean(buffer(T.SpeedOBDkmh,60)>50);
% if > 0 motor way or else freeway

%% engine RPM

EngineRPM_score = mean(-1* mean(buffer(T.EngineRPMrpm,5))>5000);
Coolant_weight1 = -1*mean(buffer(T.EngineCoolantTemperatureC,5))>85;
Coolant_weight2 = -2.5*mean(buffer(T.EngineCoolantTemperatureC,5))<85 & ...
    mean(buffer(T.EngineCoolantTemperatureC,5))>=20;

Coolant_weight3 = 3.5.*mean(buffer(T.EngineCoolantTemperatureC,5))<20; 

New_EngineRPM_Score1 = mean(Coolant_weight1+EngineRPM_score)
New_EngineRPM_Score2 = mean(Coolant_weight2+EngineRPM_score)
New_EngineRPM_Score3 = mean(Coolant_weight3+EngineRPM_score)

%% RPM anytime
EngineRPM_score_anypoint = -1.*(T.EngineRPMrpm>6000);
Coolant_weight1 = -1*(T.EngineCoolantTemperatureC>85);
Coolant_weight2 = -2.5*(T.EngineCoolantTemperatureC>=20);
Coolant_weight3 = 3.5*(T.EngineCoolantTemperatureC<20); 

New_EngineRPM_Score1_anypoint = mean(Coolant_weight1+EngineRPM_score_anypoint)
New_EngineRPM_Score2_anypoint = mean(Coolant_weight2+EngineRPM_score_anypoint)
New_EngineRPM_Score3_anypoint = mean(Coolant_weight3+EngineRPM_score_anypoint)


%% Engine load

Engineload_score = -1* mean(buffer(T.EngineLoad,10)>60);
Coolant_weight1 = -1*mean(buffer(T.EngineCoolantTemperatureC,10))>85;
Coolant_weight2 = -2.5*mean(buffer(T.EngineCoolantTemperatureC,10))<85 & ...
    mean(buffer(T.EngineCoolantTemperatureC,10))>=20;

Coolant_weight3 = 3.5.*mean(buffer(T.EngineCoolantTemperatureC,10))<20; 

New_Engineload_Score1 = mean(Coolant_weight1+Engineload_score)
New_Engineload_Score2 = mean(Coolant_weight2+Engineload_score)
New_Engineload_Score3 = mean(Coolant_weight3+Engineload_score)
     
%% Still engine load


Engineload_score = -2* mean(buffer(T.EngineLoad,10)>80);
Coolant_weight1 = -1*mean(buffer(T.EngineCoolantTemperatureC,10))>85;
Coolant_weight2 = -2.5*mean(buffer(T.EngineCoolantTemperatureC,10))<85 & ...
    mean(buffer(T.EngineCoolantTemperatureC,10))>=20;

Coolant_weight3 = 3.5.*mean(buffer(T.EngineCoolantTemperatureC,10))<20; 

New_Engineload_Score1 = mean(Coolant_weight1+Engineload_score)
New_Engineload_Score2 = mean(Coolant_weight2+Engineload_score)
New_Engineload_Score3 = mean(Coolant_weight3+Engineload_score)

%%
Engineload_score_anypoint = -1.*(T.EngineLoad>70 & T.EngineRPMrpm>1300);
Coolant_weight1 = -1*(T.EngineCoolantTemperatureC>85);
Coolant_weight2 = -2.5*(T.EngineCoolantTemperatureC>=20);
Coolant_weight3 = 3.5*(T.EngineCoolantTemperatureC<20); 

New_EngineRPM_Score1_anypoint = mean(Coolant_weight1+Engineload_score_anypoint)
New_EngineRPM_Score2_anypoint = mean(Coolant_weight2+Engineload_score_anypoint)
New_EngineRPM_Score3_anypoint = mean(Coolant_weight3+Engineload_score_anypoint)
%%